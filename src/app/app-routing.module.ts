import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AttendencesComponent } from './attendences/attendences.component';
import { NewAttendenceComponent } from './attendences/new/new.component';
import { EditEmpTypeComponent } from './emp-types/edit/edit.component';
import { EmpTypesComponent } from './emp-types/emp-types.component';
import { NewEmpTypeComponent } from './emp-types/new/new.component';
import { EditComponent } from './Employees/edit/edit.component';
import { EmployeesComponent } from './employees/employees.component';
import { NewEmployeeComponent } from './employees/new/new.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'emptypes',
    component: EmpTypesComponent
  },
  {
    path: 'emptypes/new',
    component: NewEmpTypeComponent
  },
  {
    path: 'emptypes/:id/edit',
    component: EditEmpTypeComponent
  },
  {
    path: 'employees',
    component: EmployeesComponent
  },
  {
    path: 'employees/new',
    component: NewEmployeeComponent
  },
  {
  path: 'employees/:id/edit',
  component: EditComponent
  },
  {
    path: 'attendences',
    component: AttendencesComponent
  },
  {
    path: '**',
    redirectTo: '/'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
