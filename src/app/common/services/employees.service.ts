import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { IEmployee } from '../models/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {

  constructor(private httpClient: HttpClient) { }

  getAllEmployees(): Observable<IEmployee[]> {
    return this.httpClient.get<IEmployee[]>(`${environment.apiBaseUrl}/api/employee/getall`);
  }

  getAllWithType(): Observable<IEmployee[]> {
    return this.httpClient.get<IEmployee[]>(`${environment.apiBaseUrl}/api/employee/getallwithtype`);
  }

  getEmployees(id: number): Observable<IEmployee[]> {
    return this.httpClient.get<IEmployee[]>(`${environment.apiBaseUrl}/api/employee/get?id=${id}`);
  }

  createEmployee(typeid: number, name: string): Observable<any> {
    return this.httpClient.post(`${environment.apiBaseUrl}/api/employee/create`, { emptypeid: typeid, name: name });
  }

  updateEmployee(id: number, type: string, name: string) {
    return this.httpClient.put(`${environment.apiBaseUrl}/api/employee/update?id=${id}`, { type: type, name: name });
  }

  deleteEmployee(id: number): Observable<any> {
    return this.httpClient.delete(`${environment.apiBaseUrl}/api/employee/delete?id=${id}`);
  }
}