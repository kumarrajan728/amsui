import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IEmpType } from '../models/emp-type';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class EmptypesService {

  constructor(private httpClient: HttpClient) { }

  getAllEmpTypes(): Observable<IEmpType[]> {
    return this.httpClient.get<IEmpType[]>(`${environment.apiBaseUrl}/api/emptype/getall`);
  }

  getEmpType(id: number): Observable<IEmpType> {
      return this.httpClient.get<IEmpType>(`${environment.apiBaseUrl}/api/emptype/get?id=${id}`);
  }

  createEmpType(type: string): Observable<any>{  
    return this.httpClient.post(`${environment.apiBaseUrl}/api/emptype/create`, {type: type} );
  }

  updateEmpType(id: number, type: string) {
    return this.httpClient.put(`${environment.apiBaseUrl}/api/emptype/update?id=${id}`, {type: type});
  }
  
  deleteEmpType(id: number): Observable<any> {
    return this.httpClient.delete(`${environment.apiBaseUrl}/api/emptype/delete?id=${id}`);
  }


}
