import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { IAttendence } from '../models/attendence';

@Injectable({
  providedIn: 'root'
})
export class AttendenceService {

  constructor(private hc: HttpClient) { }
  
  getAllAttendences(): Observable<IAttendence[]>{
    return this.hc.get<IAttendence[]>(`${environment.apiBaseUrl}/api/attendence/getall`);
  }
}
