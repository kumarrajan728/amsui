import { TestBed } from '@angular/core/testing';

import { EmptypesService } from './emptypes.service';

describe('EmptypesService', () => {
  let service: EmptypesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EmptypesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
