export interface IEmployee{
    id: number;
    name: string;
    type: string;
}