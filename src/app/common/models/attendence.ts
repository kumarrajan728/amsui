import { NodeWithI18n } from '@angular/compiler';

export interface IAttendence {
    id: number;
    date: Date;
    isPresent: boolean;
    empId: number;

}