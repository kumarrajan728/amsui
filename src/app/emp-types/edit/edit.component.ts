import { switchMap } from "rxjs/operators";
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmptypesService } from '../../common/services/emptypes.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditEmpTypeComponent implements OnInit {

  empTypeName: string;
  id: number;

  constructor(private ets: EmptypesService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap
      .pipe(
        switchMap(p => {
        const id = +p.get('id');
        this.id = id;
        return this.ets.getEmpType(id)
      }))
        .subscribe(t => this.empTypeName = t.type)
  }

  update() {
    this.ets.updateEmpType(this.id, this.empTypeName).subscribe();
  }
}
