import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditEmpTypeComponent } from './edit.component';

describe('EditComponent', () => {
  let component: EditEmpTypeComponent;
  let fixture: ComponentFixture<EditEmpTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditEmpTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditEmpTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
