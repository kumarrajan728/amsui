import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewEmpTypeComponent } from './new.component';

describe('NewComponent', () => {
  let component: NewEmpTypeComponent;
  let fixture: ComponentFixture<NewEmpTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewEmpTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEmpTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
