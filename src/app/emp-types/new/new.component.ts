import { Component, OnInit } from '@angular/core';
import { EmptypesService } from '../../common/services/emptypes.service';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewEmpTypeComponent implements OnInit {

  empTypeName: string;

  constructor(private ets: EmptypesService) { }

  ngOnInit(): void {
  }

  save() {
    this.ets.createEmpType(this.empTypeName)
      .subscribe(t => this.empTypeName = '');
  }
}
