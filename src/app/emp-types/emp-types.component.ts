import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IEmpType } from '../common/models/emp-type';
import { EmptypesService } from '../common/services/emptypes.service';

@Component({
  selector: 'app-emp-types',
  templateUrl: './emp-types.component.html',
  styleUrls: ['./emp-types.component.css']
})
export class EmpTypesComponent implements OnInit {

  empTypes: IEmpType[];

  constructor(private ets: EmptypesService,
    private router: Router) { }

  ngOnInit(): void {
    this.getAllEmpTypes();
  }

  editType(id: number) {
    this.router.navigate([`emptypes/${id}/edit`]);
  }

  deleteType(id: number) {
    let answer: boolean = confirm(`Are you sure you want to delete record with id: ${ id }`);

    if (answer) {
      // Call Delete api
      this.ets.deleteEmpType(id).subscribe(t => this.getAllEmpTypes());
    }
  }

  private getAllEmpTypes() {
    this.ets.getAllEmpTypes().subscribe(t => this.empTypes = t)
  }
}
