import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpTypesComponent } from './emp-types.component';

describe('EmpTypesComponent', () => {
  let component: EmpTypesComponent;
  let fixture: ComponentFixture<EmpTypesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmpTypesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
