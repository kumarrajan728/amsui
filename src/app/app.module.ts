import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { EmpTypesComponent } from './emp-types/emp-types.component';
import { EmployeesComponent } from './employees/employees.component';
import { AttendencesComponent } from './attendences/attendences.component';
import { ReportComponent } from './report/report.component';
import { EditComponent } from './Employees/edit/edit.component';
import { NewAttendenceComponent } from './attendences/new/new.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { EditEmpTypeComponent } from './emp-types/edit/edit.component';
import { NewEmpTypeComponent } from './emp-types/new/new.component';
import { NewEmployeeComponent } from './employees/new/new.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    EmpTypesComponent,
    EditEmpTypeComponent,
    NewEmpTypeComponent,
    EmployeesComponent,
    AttendencesComponent,
    ReportComponent,
    EditComponent,
    NewAttendenceComponent,
    NewEmployeeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    MatNativeDateModule,
    MatDatepickerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
