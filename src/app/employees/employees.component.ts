import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IEmployee } from '../common/models/employee';
import { EmployeesService } from '../common/services/employees.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  employees: IEmployee[];

  constructor(private emps: EmployeesService, private router: Router) { }

  ngOnInit(): void {
    this.getFreshData();
  }

  edit(id: number) {
    const url = `employees/${id}/edit`;

    this.router.navigate([url]);
  }

  delete(id: number) {
    this.emps.deleteEmployee(id)
      .subscribe(
        d => {
          alert('Employee deleted successfully');
          this.getFreshData();
        },
        e => alert(e.error.exceptionMessage)
      );
  }

  private getFreshData() {
    this.emps.getAllWithType().subscribe(t => this.employees = t);
  }
}