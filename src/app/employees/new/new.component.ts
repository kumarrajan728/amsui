import { Component, OnInit } from '@angular/core';
import { EmptypesService } from '../../common/services/emptypes.service';
import { EmployeesService } from '../../common/services/employees.service';
import { IEmpType } from '../../common/models/emp-type';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewEmployeeComponent implements OnInit {
  name: string;
  empTypeId: string = '0';
  chk = false;
  rd;
  dob;

  empTypes: IEmpType[];

  constructor(private es: EmployeesService, private empt: EmptypesService) { }

  ngOnInit(): void {
    this.empt.getAllEmpTypes().subscribe(t => this.empTypes = t);
  }

  saveMeBaby() {
    this.es.createEmployee(+this.empTypeId, this.name)
      .subscribe(t => {
        this.name = '';
        this.empTypeId = '';

        alert('Employee saved successfully');
      });
  }
}
