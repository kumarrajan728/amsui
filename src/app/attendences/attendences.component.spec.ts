import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendencesComponent } from './attendences.component';

describe('AttendencesComponent', () => {
  let component: AttendencesComponent;
  let fixture: ComponentFixture<AttendencesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AttendencesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
