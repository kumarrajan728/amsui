import { Component, OnInit } from '@angular/core';
import { IAttendence } from '../common/models/attendence';
import { AttendenceService } from '../common/services/attendence.service';

@Component({
  selector: 'app-attendences',
  templateUrl: './attendences.component.html',
  styleUrls: ['./attendences.component.css']
})
export class AttendencesComponent implements OnInit {

  attendences: IAttendence[];

  constructor(private atts: AttendenceService) { }

  ngOnInit(): void {
    this.atts.getAllAttendences().subscribe(a => this.attendences = a);
  }

}
