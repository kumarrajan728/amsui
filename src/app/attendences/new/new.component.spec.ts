import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewAttendenceComponent } from './new.component';

describe('NewComponent', () => {
  let component: NewAttendenceComponent;
  let fixture: ComponentFixture<NewAttendenceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewAttendenceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewAttendenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
